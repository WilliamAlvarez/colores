﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace colores
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page1 : ContentPage
	{
		public Page1 ()
		{
			InitializeComponent ();
		}

        async private void ButtonClickedR(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new MainPage());
        }

        void OnSliderValueChanged(object sender, ValueChangedEventArgs args)
        {
            id_bv_backgorund.WidthRequest = (int)id_sl_width.Value;
            id_bv_backgorund.HeightRequest = (int)id_sl_hight.Value;

            if (sender == id_sl_width)
            {
                id_lb_width.Text = id_sl_width.Value.ToString();
            }
            else if (sender == id_sl_hight)
            {
                id_lb_hight.Text = id_sl_hight.Value.ToString();
            }
        }

    }
}